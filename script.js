//ESTABLECE DE MOVIDA EL CÁLCULO A MOSTRAR Y LO MUESTRA
let resultado;

function calculaCaptcha() {
    let parrafo = document.querySelector("#calculo");
    let valor1 = Math.floor(Math.random() * 10 + 0.5);
    let valor2 = Math.floor(Math.random() * 10 + 0.5);
    resultado = valor1 + valor2;
    console.log(valor1, " - ", valor2);
    parrafo.innerHTML = `${valor1} + ${valor2} = ?*`;
}

function validaNombre() {
    let nombre = document.querySelector("#nombre").value;
    if (nombre.trim() == "") {
        alert("el nombre está vacío");
        document.querySelector("#nombre").focus();
        return false;   //SI NO INGRESÓ NADA RETORNAMOS FALSE
    } else {
        return true;    //SINO TRUE
    }
}


function validar() {
    //VALIDA NOMBRE
    // let nombre = document.querySelector("#nombre").value;
    // if (nombre.trim() == "") {
    //     alert("el nombre está vacío");
    //     document.querySelector("#nombre").focus();
    //     return;
    // }
    //COMO EJEMPLO, SI QUISIÉRAMOS PONER LA VALIDACIÓN EN UNA FUNCIÓN,
    //PREGUNTARÍAMOS SI ANDUVO BIEN O NO PARA SALIR DE VALIDAR(), 
    //COMO ESTÁ A CONTINUACIÓN.

    if (!validaNombre()) return;    //SI VALIDANOMBRE() RETORNÓ FALSE SALE DE VALIDAR()

    //Y ASÍ PODRÍAMOS HACER CON LOS DEMÁS ELEMENTOS A VALIDAR...
    //NO OBSTANTE, DEJÉ TODO JUNTO PARA QUE RESULTE MÁS FACIL DE ENTENDER

    //VALIDA EMAIL
    let email = document.querySelector("#email").value;
    const validRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //EXPRESIONES REGULARES
    if (!email.match(validRegEx)) {
        alert("el email es incorrecto");
        document.querySelector("#email").focus();
        return;
    }

    let telefono = document.querySelector("#telefono").value;
    const validtel = /^\(?(\d{2})\)?[-]?(\d{4})[-]?(\d{4})$/;
    if (!telefono.match(validtel)){
        alert("Tienes que completar telefono");
        document.querySelector("#telefono").focus();
        return;
    }

    //VALIDA CAPTCHA
    let inputResul = document.querySelector("#captcha").value;
    if (inputResul.trim() === "") {
        alert("Tienes que completar el cálculo");
        document.querySelector("#captcha").focus();
        return;
    }
    if (isNaN(inputResul)) {
        alert("No es un número el que ingresaste");
        document.querySelector("#captcha").focus();
        return;
    }
    inputResul = parseFloat(inputResul);

    if (resultado !== inputResul) {
        alert("No es el valor correcto");
        document.querySelector("#captcha").focus();
        return;
    }


    alert("COMPLETADO CORRECTAMENTE");
}

function main() {
    calculaCaptcha();
}

main()


//  EXPRESION REGULAR PARA VALIDAR TELEFONO / ^ \ (? (\ d {3}) \)? [-]? (\ d {3}) [-]? (\ d {4}) $ /

// Para comprender mejor lo que implica, aquí hay un desglose de todos los elementos.

//     / ^ \ (?: Una de las formas opcionales del número puede comenzar con un paréntesis abierto.
//     (\ d {3}): Entonces debe incluir tres dígitos numéricos. Si no hay paréntesis, el número debe comenzar con estos dígitos, por ejemplo - (541 o 541.
//     \) ?: Aqui se proporciona una opción para incluir un paréntesis cerrado.
//     [-] ?: La cadena puede contener opcionalmente un guión después del paréntesis o después de los primeros tres dígitos.
//     (\ d {3}): Entonces el número debe contener otros tres dígitos. Dependiendo de sus opciones anteriores, puede verse como (541) -753, 541-753 o 541753.
//     [-] ?: Una vez más, puede incluir un guión opcional al final - (541) -753-, 541-753- o 541753-.
//     (\ d {4}) $ /: Por último, la secuencia debe terminar con una secuencia de cuatro dígitos. Puede verse como (541) -753-6010, 541-753-6010, 541753-6010 o 541753-6010.
